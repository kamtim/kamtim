const pkg = require('./package');

module.exports = {
  "webpackConfig": {
    "output": {
      "publicPath": `/static/kamtim/${pkg.version}/`
    },
    "module": {
      "rules": [
        { parser: { system: false } },
        {
          test: /\.tsx?$/,
          loader: "awesome-typescript-loader"
        },
        {
          test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/,
          loader: "file-loader"
        },
        {
          "test": /\.css$/,
          "use": ['style-loader', 'css-loader']
        },
      ],
    },
  },
  "navigations": {
    "repos": "/kamtim"
  },
  config: {
    "kamtim.api.base": "/api"
  }
};