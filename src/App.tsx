import React from 'react';
import { Provider } from 'react-redux'
import {createBrowserHistory} from 'history'
import {Route, Router, Switch} from "react-router-dom"
import {Page} from './components/Page/Page';
import {prefixPath} from './constants/constants';

import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import {reducer} from './redux/commonReducers'

import {
    rootSaga
} from "./redux/sagas";

import 'antd/dist/antd.css';
import {RecipesPageConnected} from './components/RecipesPage/RecipesPage.container';
import {RecipeConnected} from "@main/components/Recipe/Recipe.container";

const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, undefined, composeWithDevTools(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(rootSaga);

export const App = () => {
    return (
        <Provider store={store}>
            <Router history={history}>
                <Switch>
                    <Route exact history={history} path={prefixPath} component={() => <Page><RecipesPageConnected/></Page>}/>
                    <Route history={history} path={`${prefixPath}/recipe/:recipeId`} component={(props) => <Page><RecipeConnected {...props}/></Page>}/>
                </Switch>
            </Router>
        </Provider>
    )
};