import {getRecipeRoutine, getRecipesOptionsRoutine, getRecipesRoutine} from "@main/redux/actions";
import {recipeReducer, defaultState} from './reducers';

const getRecipe: (id: number) => Recipe = (id) => ({
    id,
    title: `Cool recipe ${id}`,
    image: 'https://spoonacular.com/recipeImages/654812-556x370.jpg',
    imageType: 'jpg',
});

const recipes = [
    getRecipe(1),
    getRecipe(2),
    getRecipe(3),
];

const options = [
    {
        label: 'pizza',
        value: 45678,
    },
    {
        label: 'pizza with cucumber',
        value: 45678,
    }
];

const recipe: ExtendedRecipe = {
    id: 100,
    image: '',
    imageType: '',
    title: 'Good recipe',

    analyzedInstructions: [],
    summary: '',
    extendedIngredients: [],

    healthScore: 10,
    readyInMinutes: 45,
    servings: 6,

    cheap: false,
    dairyFree: false,
    glutenFree: true,
    vegan: true,
    vegetarian: false,
    veryHealthy: true,
    veryPopular: false,
};

describe('reducers', () => {
    it('getRecipesRoutine.SUCCESS', () => {
        expect(recipeReducer(defaultState, {
            type: getRecipesRoutine.SUCCESS,
            payload: recipes,
            error: false,
        })).toEqual({
            ...defaultState,
            recipes,
        });
    });

    it('getRecipesOptionsRoutine.SUCCESS', () => {
        expect(recipeReducer(defaultState, {
            type: getRecipesOptionsRoutine.SUCCESS,
            payload: options,
            error: false,
        })).toEqual({
            ...defaultState,
            recipesOptions: options,
        });
    });

    it('getRecipeRoutine.SUCCESS', () => {
        expect(recipeReducer(defaultState, {
            type: getRecipeRoutine.SUCCESS,
            payload: recipe,
            error: false,
        })).toEqual({
            ...defaultState,
            recipe,
        });
    })
});