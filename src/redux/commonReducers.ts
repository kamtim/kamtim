import { combineReducers } from 'redux';
import recipeReducer from './reducers';

export const loadingReducer = (state, action) => {
    const {type} = action;

    const [prefix, stage] = type.split('/');

    if (!prefix || !['REQUEST', 'FULFILL'].includes(stage)) {
        return state === undefined ? {} : state;
    }

    return {
        ...state,
        [prefix]: stage === 'REQUEST',
    };
};

export const errorReducer = (state, action) => {
    const {type, payload} = action;

    const [prefix, stage] = type.split('/');

    if (!prefix || !['REQUEST', 'FAILURE'].includes(stage)) {
        return state === undefined ? {} : state;
    }

    return {
        ...state,
        [prefix]: stage === 'FAILURE' ? payload : null,
    };
};

export const reducer = combineReducers({
    loadingReducer,
    errorReducer,
    recipeReducer,
});