import {call, debounce, put, takeEvery} from 'redux-saga/effects'
import {getIngredientsOptionsApi, getRecipeApi, getRecipesApi, getRecipesOptionsApi} from './api';
import {
    getIngredientsOptions,
    getIngredientsOptionsWatcher,
    getRecipe, getRecipes, getRecipesOptions, getRecipesOptionsWatcher, getRecipesWatcher, getRecipeWatcher} from './sagas';
import {
    getIngredientsOptionsRoutine,
    getRecipeRoutine,
    getRecipesOptionsRoutine,
    getRecipesRoutine
} from "@main/redux/actions";

const state = {recipes: []};

const recipes: Recipe[] = [{id: 100, image: '', imageType: '', title: 'recipe title'}];
const wrappedRecipes = {
    data: {
        results: recipes,
    }
};

const rawOptions = {
    data: [
        {
            title: 'pizza',
            id: 45678,
        },
        {
            title: 'pizza with cucumber',
            id: 45678,
        }
    ]
};

const rawIngredientsOptions = {
    data: [
        {
            name: 'apple',
        },
        {
            name: 'banana',
        }
    ]
};

const ingredientsOptions = [
    {
        label: 'apple',
        value: 'apple',
    },
    {
        label: 'banana',
        value: 'banana',
    }
];

const options = [
    {
        label: 'pizza',
        value: 45678,
    },
    {
        label: 'pizza with cucumber',
        value: 45678,
    }
];

const recipe: ExtendedRecipe = {
    id: 100,
    image: '',
    imageType: '',
    title: 'Good recipe',

    analyzedInstructions: [],
    summary: '',
    extendedIngredients: [],

    healthScore: 10,
    readyInMinutes: 45,
    servings: 6,

    cheap: false,
    dairyFree: false,
    glutenFree: true,
    vegan: true,
    vegetarian: false,
    veryHealthy: true,
    veryPopular: false,
};

describe('getProducts Saga test', () => {
    describe('getRecipes', () => {
        it('watcher', () => {
            const gen = getRecipesWatcher();

            expect(gen.next().value)
                .toEqual(takeEvery(getRecipesRoutine.TRIGGER, getRecipes));
        });

        it('success', () => {
            const gen = getRecipes({payload: {query: 'testRecipe', includeIngredients: 'apple'}});

            expect(gen.next().value)
                .toEqual(put(getRecipesRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getRecipesApi, {query: 'testRecipe', includeIngredients: 'apple'}));

            expect(gen.next(wrappedRecipes).value)
                .toEqual(put(getRecipesRoutine.success(recipes)));

            expect(gen.next().value)
                .toEqual(put(getRecipesRoutine.fulfill()));
        });

        it('error', () => {
            const gen = getRecipes({payload: {query: 'testRecipe', includeIngredients: 'apple'}});

            expect(gen.next().value)
                .toEqual(put(getRecipesRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getRecipesApi, {query: 'testRecipe', includeIngredients: 'apple'}));

            const error = new TypeError('Cannot read property \'data\' of undefined');

            expect(gen.next().value)
                .toEqual(put(getRecipesRoutine.failure(error)));

            expect(gen.next().value)
                .toEqual(put(getRecipesRoutine.fulfill()));
        });
    });

    describe('getRecipesOptions', () => {
        it('watcher', () => {
            const gen = getRecipesOptionsWatcher();

            expect(gen.next().value)
                .toEqual(debounce(250, getRecipesOptionsRoutine.TRIGGER, getRecipesOptions));
        });

        it('success', () => {
            const gen = getRecipesOptions({payload: 'testRecipeForOptions'});

            expect(gen.next().value)
                .toEqual(put(getRecipesOptionsRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getRecipesOptionsApi, 'testRecipeForOptions'));

            expect(gen.next(rawOptions).value)
                .toEqual(put(getRecipesOptionsRoutine.success(options)));

            expect(gen.next().value)
                .toEqual(put(getRecipesOptionsRoutine.fulfill()));
        });

        it('error', () => {
            const gen = getRecipesOptions({payload: 'testRecipeForOptions'});

            expect(gen.next().value)
                .toEqual(put(getRecipesOptionsRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getRecipesOptionsApi, 'testRecipeForOptions'));

            const error = new TypeError('Cannot read property \'data\' of undefined');

            expect(gen.next().value)
                .toEqual(put(getRecipesOptionsRoutine.failure(error)));

            expect(gen.next().value)
                .toEqual(put(getRecipesOptionsRoutine.fulfill()));
        });
    })

    describe('getRecipe', () => {
        it('watcher', () => {
            const gen = getRecipeWatcher();

            expect(gen.next().value)
                .toEqual(takeEvery(getRecipeRoutine.TRIGGER, getRecipe));
        });

        it('success', () => {
            const recipeId = '200';

            const gen = getRecipe({payload: recipeId});

            expect(gen.next().value)
                .toEqual(put(getRecipeRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getRecipeApi, recipeId));

            expect(gen.next({data: recipe}).value)
                .toEqual(put(getRecipeRoutine.success(recipe)));

            expect(gen.next().value)
                .toEqual(put(getRecipeRoutine.fulfill()));
        });

        it('error', () => {
            const recipeId = '200';

            const gen = getRecipe({payload: recipeId});

            expect(gen.next().value)
                .toEqual(put(getRecipeRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getRecipeApi, recipeId));

            const error = new TypeError('Cannot read property \'data\' of undefined');

            expect(gen.next().value)
                .toEqual(put(getRecipeRoutine.failure(error)));

            expect(gen.next().value)
                .toEqual(put(getRecipeRoutine.fulfill()));
        });
    })

    describe('getIngredientsOptions', () => {
        it('watcher', () => {
            const gen = getIngredientsOptionsWatcher();

            expect(gen.next().value)
                .toEqual(debounce(250, getIngredientsOptionsRoutine.TRIGGER, getIngredientsOptions));
        });

        it('success', () => {
            const gen = getIngredientsOptions({payload: 'testIngredientsForOptions'});

            expect(gen.next().value)
                .toEqual(put(getIngredientsOptionsRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getIngredientsOptionsApi, 'testIngredientsForOptions'));

            expect(gen.next(rawIngredientsOptions).value)
                .toEqual(put(getIngredientsOptionsRoutine.success(ingredientsOptions)));

            expect(gen.next().value)
                .toEqual(put(getIngredientsOptionsRoutine.fulfill()));
        });

        it('error', () => {
            const gen = getIngredientsOptions({payload: 'testIngredientsForOptions'});

            expect(gen.next().value)
                .toEqual(put(getIngredientsOptionsRoutine.request()));

            expect(gen.next().value)
                .toEqual(call(getIngredientsOptionsApi, 'testIngredientsForOptions'));

            const error = new TypeError('Cannot read property \'data\' of undefined');

            expect(gen.next().value)
                .toEqual(put(getIngredientsOptionsRoutine.failure(error)));

            expect(gen.next().value)
                .toEqual(put(getIngredientsOptionsRoutine.fulfill()));
        });
    })
})