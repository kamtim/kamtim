import {api} from "@main/redux/requests";

export const getRecipesApi = ({query, includeIngredients}: {query: string, includeIngredients: string}) => {
    return api.get(`/recipes/complexSearch`,
        {params: {number: 100, query, includeIngredients }}
    )
};

export const getRecipesOptionsApi = (value: string) => {
    return api.get(`/recipes/autocomplete`,
        {params: {number: 10, query: value}}
    )
};

export const getRecipeApi = (recipeId: string) => {
    return api.get(`/recipes/${recipeId}/information`);
};

export const getIngredientsOptionsApi = (value: string) => {
    return api.get(`/food/ingredients/autocomplete`,
        {params: {number: 10, query: value}}
    )
};