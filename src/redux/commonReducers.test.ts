import {createRoutine} from "redux-saga-routines";
import {loadingReducer, errorReducer} from "@main/redux/commonReducers";

describe('commonReducers', () => {
    describe('loadingReducer', () => {
        const EXAMPLE = 'EXAMPLE';
        const exampleRoutine = createRoutine(EXAMPLE);

        it('Should not handle loading in TRIGGER action', () => {
            expect(loadingReducer({}, {
                type: exampleRoutine.TRIGGER,
            })).toEqual({})
        });

        it('Should handle loading: true in REQUEST action', () => {
            expect(loadingReducer({}, {
                type: exampleRoutine.REQUEST,
            })).toEqual({[EXAMPLE]: true})
        });

        it('Should not handle loading in SUCCESS action', () => {
            expect(loadingReducer({}, {
                type: exampleRoutine.SUCCESS,
            })).toEqual({})
        });

        it('Should not handle loading in FAILURE action', () => {
            expect(loadingReducer({}, {
                type: exampleRoutine.FAILURE,
            })).toEqual({})
        });

        it('Should handle loading: false in FULFILL action', () => {
            expect(loadingReducer({[EXAMPLE]: true}, {
                type: exampleRoutine.FULFILL,
            })).toEqual({[EXAMPLE]: false})
        });

        it('Should not handle loading in another action', () => {
            expect(loadingReducer(undefined, {
                type: 'ANOTHER_ACTION',
            })).toEqual({})
        });
    })

    describe('errorReducer', () => {
        const EXAMPLE = 'EXAMPLE';
        const exampleRoutine = createRoutine(EXAMPLE);

        it('Should not handle loading in TRIGGER action', () => {
            expect(errorReducer({}, {
                type: exampleRoutine.TRIGGER,
            })).toEqual({})
        });

        it('Should handle loading: true in REQUEST action', () => {
            expect(errorReducer({}, {
                type: exampleRoutine.REQUEST,
            })).toEqual({[EXAMPLE]: null})
        });

        it('Should not handle loading in SUCCESS action', () => {
            expect(errorReducer({}, {
                type: exampleRoutine.SUCCESS,
            })).toEqual({})
        });

        it('Should not handle loading in FAILURE action', () => {
            const error = new Error('Error');

            expect(errorReducer({[EXAMPLE]: null}, {
                type: exampleRoutine.FAILURE,
                payload: error,
            })).toEqual({[EXAMPLE]: error})
        });

        it('Should handle loading: false in FULFILL action', () => {
            expect(errorReducer({}, {
                type: exampleRoutine.FULFILL,
            })).toEqual({})
        });

        it('Should not handle loading in another action', () => {
            expect(errorReducer(undefined, {
                type: 'ANOTHER_ACTION',
            })).toEqual({})
        });
    })
});