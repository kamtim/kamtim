import {selectErrors, selectIsLoading} from "./selectors"

describe('selectors', () => {
    it('Should check selectIsLoading', () => {
        const state = {loadingReducer: {ROUTINE_NAME: true}};

        expect(selectIsLoading(state, 'ROUTINE_NAME')).toEqual(true);
    });

    it('Should check selectIsLoading', () => {
        const state = {errorReducer: {ROUTINE_NAME: new Error('Error')}};

        expect(selectErrors(state, 'ROUTINE_NAME')).toEqual(new Error('Error'));
    });
});