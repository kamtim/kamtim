import {createRoutine} from 'redux-saga-routines';

export const RECIPES_GET = 'RECIPES_GET';
export const getRecipesRoutine = createRoutine(RECIPES_GET);

export const RECIPES_OPTIONS_GET = 'RECIPES_OPTIONS_GET';
export const getRecipesOptionsRoutine = createRoutine(RECIPES_OPTIONS_GET);

export const RECIPE_GET = 'RECIPE_GET';
export const getRecipeRoutine = createRoutine(RECIPE_GET);

export const INGREDIENTS_OPTIONS_GET = 'INGREDIENTS_OPTIONS_GET';
export const getIngredientsOptionsRoutine = createRoutine(INGREDIENTS_OPTIONS_GET);