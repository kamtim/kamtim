import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { getConfig } from '@ijl/cli';
import {apiKey} from "@main/constants/constants";

const axiosConfig: AxiosRequestConfig = {
    baseURL: getConfig()['kamtim.api.base'],
    params: {
        apiKey,
    },
};

export const api: AxiosInstance = axios.create(axiosConfig);