import {handleActions} from 'redux-actions';
import {
    getIngredientsOptionsRoutine,
    getRecipeRoutine,
    getRecipesOptionsRoutine,
    getRecipesRoutine
} from "@main/redux/actions";

export const defaultState: Store = {
    recipes: [],
    recipesOptions: [],
    ingredientsOptions: [],
    recipe: undefined,
};

interface Store {
    recipes: Recipe[],
    recipesOptions: {label: string, value: string}[],
    ingredientsOptions: {label: string, value: string}[],
    recipe?: ExtendedRecipe,
}

type PayloadUnion = {}

export const recipeReducer = handleActions<PayloadUnion>(
    {
        [getRecipesRoutine.SUCCESS]: (
            state,
            {payload}
        ) => {
            return {
                ...state,
                recipes: payload,
            };
        },
        [getRecipesOptionsRoutine.SUCCESS]: (
            state,
            {payload}
        ) => {
            return {
                ...state,
                recipesOptions: payload,
            };
        },
        [getRecipeRoutine.SUCCESS]: (
            state,
            {payload}
        ) => {
            return {
                ...state,
                recipe: payload,
            };
        },
        [getIngredientsOptionsRoutine.SUCCESS]: (
            state,
            {payload}
        ) => {
            return {
                ...state,
                ingredientsOptions: payload,
            };
        },
    },
    defaultState,
);

export default recipeReducer;