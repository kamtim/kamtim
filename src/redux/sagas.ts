import { all, takeEvery, put, call, debounce } from 'redux-saga/effects'
import { message } from 'antd';
import {getRecipesRoutine, getRecipesOptionsRoutine, getRecipeRoutine, getIngredientsOptionsRoutine} from './actions';
import {getRecipesApi, getRecipesOptionsApi, getRecipeApi, getIngredientsOptionsApi} from "@main/redux/api";

const showErrorMessage = error => {
    message.error(error.message);
};

export function* getRecipes(action) {
    try {
        yield put(getRecipesRoutine.request());
        const {data: {results}} = yield call(getRecipesApi, action.payload);

        yield put(getRecipesRoutine.success(results));
    } catch (error) {
        yield put(getRecipesRoutine.failure(error));
        showErrorMessage(error);
    } finally {
        yield put(getRecipesRoutine.fulfill());
    }
}

export function* getRecipesWatcher() {
    yield takeEvery(getRecipesRoutine.TRIGGER, getRecipes);
}

export function* getRecipesOptions(action) {
    try {
        yield put(getRecipesOptionsRoutine.request());
        const {data: results} = yield call(getRecipesOptionsApi, action.payload);

        const recipesOptions = results.map(({title, id}) => ({
            label: title,
            value: id,
        }));

        yield put(getRecipesOptionsRoutine.success(recipesOptions));
    } catch (error) {
        yield put(getRecipesOptionsRoutine.failure(error));
        showErrorMessage(error);
    } finally {
        yield put(getRecipesOptionsRoutine.fulfill());
    }
}

export function* getRecipesOptionsWatcher() {
    yield debounce(250, getRecipesOptionsRoutine.TRIGGER, getRecipesOptions);
}


export function* getRecipe(action) {
    try {
        yield put(getRecipeRoutine.request());
        const {data: results} = yield call(getRecipeApi, action.payload);

        yield put(getRecipeRoutine.success(results));
    } catch (error) {
        yield put(getRecipeRoutine.failure(error));
        showErrorMessage(error);
    } finally {
        yield put(getRecipeRoutine.fulfill());
    }
}

export function* getRecipeWatcher() {
    yield takeEvery(getRecipeRoutine.TRIGGER, getRecipe);
}

export function* getIngredientsOptions(action) {
    try {
        yield put(getIngredientsOptionsRoutine.request());
        const {data: results} = yield call(getIngredientsOptionsApi, action.payload);

        const ingredientsOptions = results.map(({name}) => ({
            label: name,
            value: name,
        }));

        yield put(getIngredientsOptionsRoutine.success(ingredientsOptions));
    } catch (error) {
        yield put(getIngredientsOptionsRoutine.failure(error));
        showErrorMessage(error);
    } finally {
        yield put(getIngredientsOptionsRoutine.fulfill());
    }
}

export function* getIngredientsOptionsWatcher() {
    yield debounce(250, getIngredientsOptionsRoutine.TRIGGER, getIngredientsOptions);
}

export function* rootSaga() {
    yield all([
        getRecipesWatcher(),
        getRecipesOptionsWatcher(),
        getRecipeWatcher(),
        getIngredientsOptionsWatcher(),
    ]);
}