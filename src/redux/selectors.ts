export const selectIsLoading = (state, routineName) => {
    return state.loadingReducer[routineName];
};

export const selectErrors = (state, routineName) => {
    return state.errorReducer[routineName];
};