import {AutoComplete, Button, Input, Tag} from "antd";
import React from "react";
import {cnRecipesPage} from "@main/components/RecipesPage/RecipesPage";

export const Filters = ({
    options,
    onSelect,
    onSearch,
    onClick,

    ingredientValue,
    setIngredientValue,

    ingredientsOptions,
    chosenIngredients,
    onIngredientSelect,
    onIngredientSearch,
    onIngredientDelete,
    nameValue,
    setNameValue,
}) => (
    <div className={cnRecipesPage('Filters')}>
        <div className={cnRecipesPage('Filter', {type: 'search'})}>
            <label
                className={cnRecipesPage('Label',{type: 'search'})}
                htmlFor="name"
            >
                Name:
            </label>
            {
                // @ts-ignore
                <AutoComplete
                    id="name"
                    value={nameValue}
                    onChange={setNameValue}
                    className={cnRecipesPage('Input', {type: 'search'})}
                    options={options}
                    onSelect={onSelect}
                    onSearch={onSearch}
                >
                    <Input
                        placeholder="Apple pie"
                    />
                </AutoComplete>
            }
        </div>
        <div className={cnRecipesPage('Filter', {type: 'ingredients'})}>
            <label
                className={cnRecipesPage('Label', {type: 'ingredients'})}
                htmlFor="ingredients"
            >
                Ingredients:
            </label>
            {
                // @ts-ignore
                <AutoComplete
                    id="ingredients"
                    value={ingredientValue}
                    onChange={setIngredientValue}
                    className={cnRecipesPage('Input', {type: 'ingredients'})}
                    options={ingredientsOptions}
                    onSelect={onIngredientSelect}
                    onSearch={onIngredientSearch}
                >
                    <Input
                        placeholder="Apple"
                    />
                </AutoComplete>
            }
            <div className={cnRecipesPage('ChosenIngredients')}>
                {
                    chosenIngredients.map(name => (
                        <Tag
                            color="#108ee9"
                            closable
                            onClose={e => {
                                e.preventDefault();
                                onIngredientDelete(name);
                            }}
                        >
                            {name}
                        </Tag>
                    ))
                }
            </div>
        </div>
        <Button
            className={cnRecipesPage('FilterButton')}
            type="primary"
            onClick={onClick}
        >
            Find
        </Button>
    </div>
)
