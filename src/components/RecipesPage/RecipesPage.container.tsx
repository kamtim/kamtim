import React, {useCallback, useState} from 'react';
import { connect } from 'react-redux';

import {RecipesPage} from "@main/components/RecipesPage/RecipesPage";
import {
    getIngredientsOptionsRoutine,
    getRecipesOptionsRoutine,
    getRecipesRoutine,
    RECIPES_GET
} from "@main/redux/actions";
import {selectIsLoading} from "@main/redux/selectors";

export const RecipesPageContainer = ({
    recipes,
    recipesOptions,
    recipesLoading,
    ingredientsOptions,

    getRecipes,
    getRecipesOptions,
    getIngredientsOptions,
}) => {
    const [nameValue, setNameValue] = useState('');
    const [ingredientValue, setIngredientValue] = useState('');
    const [chosenIngredients, setChosenIngredients] = useState([]);

    const handleClick = useCallback(() => {
        if (!nameValue && (!chosenIngredients || !chosenIngredients.length)) {
            return;
        }

        getRecipes({query: nameValue, includeIngredients: chosenIngredients.join(',')});
    }, [getRecipes, chosenIngredients]);

    const onSelect = useCallback((id: string) => {
        // @ts-ignore
        window.location = `/kamtim/recipe/${id}`;
    }, []);

    const handleSearch = useCallback((value) => {
        if (!value) {
            return;
        }

        getRecipesOptions(value);
    }, []);

    const onIngredientSelect = useCallback((val: string) => {
        setChosenIngredients(ingredients => {
            if (ingredients.find(item => item === val)) {
                return ingredients;
            }

            return [...ingredients, val];
        });

        setIngredientValue('');
    }, []);

    const handleIngredientSearch = useCallback((value) => {
        if (!value) {
            return;
        }

        getIngredientsOptions(value);
    }, []);

    const handleIngredientDelete = useCallback(ingredientForDelete => {
        setChosenIngredients(ingredients => ingredients.filter(item => item !== ingredientForDelete))
    }, []);

    return (
        <RecipesPage
            options={recipesOptions}
            recipes={recipes}
            recipesLoading={recipesLoading}
            nameValue={nameValue}

            onSelect={onSelect}
            onSearch={handleSearch}
            onClick={handleClick}
            setNameValue={setNameValue}

            ingredientValue={ingredientValue}
            ingredientsOptions={ingredientsOptions}
            chosenIngredients={chosenIngredients}

            setIngredientValue={setIngredientValue}
            onIngredientSelect={onIngredientSelect}
            onIngredientSearch={handleIngredientSearch}
            onIngredientDelete={handleIngredientDelete}
        />
    );
};

const mapStateToProps = state => {
    const {recipeReducer: {recipes, recipesOptions, ingredientsOptions}} = state;
    const recipesLoading = selectIsLoading(state, RECIPES_GET);

    return {
        recipes,
        recipesOptions,
        recipesLoading,
        ingredientsOptions,
    };
};

const mapDispatchToProps = {
    getRecipes: getRecipesRoutine.trigger,
    getRecipesOptions: getRecipesOptionsRoutine.trigger,
    getIngredientsOptions: getIngredientsOptionsRoutine.trigger,
};

export const RecipesPageConnected = connect(mapStateToProps, mapDispatchToProps)(RecipesPageContainer);
