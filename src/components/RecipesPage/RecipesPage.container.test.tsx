import React from 'react';
import {mount} from 'enzyme';
import {RecipesPageContainer} from "@main/components/RecipesPage/RecipesPage.container";
import { act } from 'react-dom/test-utils';
import {RecipesPage} from './RecipesPage';


describe('Recipes', () => {
    const getRecipes = jest.fn();
    const getRecipesOptions = jest.fn();
    const getIngredientsOptions = jest.fn();

    const wrapper = mount(
        <RecipesPageContainer
            getRecipes={getRecipes}
            recipes={[]}
            getRecipesOptions={getRecipesOptions}
            getIngredientsOptions={getIngredientsOptions}
            recipesOptions={[]}
            recipesLoading={false}
            ingredientsOptions={[{val: 'apple', text: 'apple'}]}
        />
    );

    const recipePage = wrapper.find(RecipesPage);

    it('Should call getRecipesOptions', done => {
        recipePage.prop('onSearch')('value');

        // из-за debounce
        setTimeout(() => {
            wrapper.update();
            expect(getRecipesOptions).toHaveBeenCalledWith('value');

            done();
        }, 0)
    });

    it('Should not call getRecipesOptions', done => {
        recipePage.prop('onSearch')('');

        setTimeout(() => {
            wrapper.update();
            expect(getRecipesOptions).toHaveBeenCalledTimes(1);

            done();
        }, 0)
    });

    it('Should call getIngredientsOptions', done => {
        recipePage.prop('onIngredientSearch')('appl');

        setTimeout(() => {
            wrapper.update();
            expect(getIngredientsOptions).toHaveBeenCalledTimes(1);

            done();
        }, 0)
    });

    it('Should set chosen ingredients', done => {
        act(() => {
            recipePage.prop('onIngredientSelect')('apple');
        });

        setTimeout(() => {
            wrapper.update();
            expect(wrapper.find(RecipesPage).prop('chosenIngredients')).toEqual(['apple']);

            done();
        }, 0)
    });

    it('Should not set chosen ingredients', done => {
        act(() => {
            recipePage.prop('onIngredientSelect')('apple');
        });

        setTimeout(() => {
            wrapper.update();
            expect(wrapper.find(RecipesPage).prop('chosenIngredients')).toEqual(['apple']);

            done();
        }, 0)
    });

    it('Should delete apple from chosen ingredients', done => {
        act(() => {
            recipePage.prop('onIngredientDelete')('apple');
        });

        setTimeout(() => {
            wrapper.update();
            expect(wrapper.find(RecipesPage).prop('chosenIngredients')).toEqual([]);

            done();
        }, 0)
    });
});