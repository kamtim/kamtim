import React from 'react';
import {cn} from '@bem-react/classname';

import {Recipes} from '@main/components/Recipes/Recipes';

import './RecipesPage.css';

import {Spin} from 'antd';
import {Filters} from './Filters/Filters';

export const cnRecipesPage = cn('RecipesPage');

export const RecipesPage = props => {
    const {
        options,
        recipes,
        recipesLoading,
        nameValue,

        onSelect,
        onSearch,
        onClick,
        setNameValue,

        ingredientValue,
        ingredientsOptions,
        chosenIngredients,

        setIngredientValue,
        onIngredientSelect,
        onIngredientSearch,
        onIngredientDelete,
    } = props;

    return (
        <div className={cnRecipesPage()}>
            <Filters
                options={options}
                onSelect={onSelect}
                onSearch={onSearch}
                onClick={onClick}
                ingredientValue={ingredientValue}
                setIngredientValue={setIngredientValue}
                ingredientsOptions={ingredientsOptions}
                chosenIngredients={chosenIngredients}
                onIngredientSelect={onIngredientSelect}
                onIngredientSearch={onIngredientSearch}
                onIngredientDelete={onIngredientDelete}
                nameValue={nameValue}
                setNameValue={setNameValue}
            />
            {
                recipesLoading ?
                    <Spin className={cnRecipesPage('Spin')} tip="Loading..." /> :
                    <Recipes recipes={recipes}/>
            }
        </div>
    );
};