import React from 'react';
import {shallow} from 'enzyme';
import {RecipesPage} from "./RecipesPage";

export const getRecipe: (id: number) => Recipe = (id) => ({
    id,
    title: `Cool recipe ${id}`,
    image: 'https://spoonacular.com/recipeImages/654812-556x370.jpg',
    imageType: 'jpg',
});

export const recipes = [
    getRecipe(1),
    getRecipe(2),
    getRecipe(3),
];

export const options = [
    {
        label: 'pizza',
        value: 45678,
    },
    {
        label: 'pizza with cucumber',
        value: 45678,
    }
];

describe('RecipesPage', () => {
    it('Should render component with empty recipes', () => {
        const wrapper = shallow(
            <RecipesPage
                recipes={[]}
                chosenIngredients={[]}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('Should render component recipes and options', () => {
        const wrapper = shallow(
            <RecipesPage
                options={options}
                recipes={recipes}
                recipesLoading={false}
                onSelect={jest.fn()}
                handleSearch={jest.fn()}
                handleClick={jest.fn()}

                ingredientValue={'appl'}
                ingredientsOptions={['apple', 'apple jelli']}
                chosenIngredients={['apple']}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('Should render component when recipes loading', () => {
        const wrapper = shallow(
            <RecipesPage
                options={options}
                recipes={recipes}
                recipesLoading={true}
                onSelect={jest.fn()}
                handleSearch={jest.fn()}
                handleClick={jest.fn()}

                ingredientValue={'appl'}
                ingredientsOptions={['apple', 'apple jelli']}
                chosenIngredients={['apple']}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });
});