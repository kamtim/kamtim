import React from 'react';
import {cn} from '@bem-react/classname';
import { Alert } from 'antd';

import {RecipeCard} from "@main/components/RecipeCard/RecipeCard";

import './Recipes.css';

const cnRecipes = cn('Recipes');

export const Recipes = ({recipes}) => {
    return (
        <div className={cnRecipes()}>
            {
                recipes.length ?
                recipes.map(({title, image, id}) => <RecipeCard title={title} image={image} id={id} />) :
                    <Alert
                        className={cnRecipes('Info')}
                        message="No results"
                        description="Please, make sure that the entered data is on English, and that it is a full word or combination"
                        type="info"
                        showIcon
                    />
            }
        </div>
    )
};