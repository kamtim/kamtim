import React from 'react';
import {shallow} from 'enzyme';
import {Recipes} from "./Recipes";

const getRecipe: (id: number) => Recipe = (id) => ({
    id,
    title: `Cool recipe ${id}`,
    image: 'https://spoonacular.com/recipeImages/654812-556x370.jpg',
    imageType: 'jpg',
});

describe('Recipes', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Recipes
                recipes={[
                    getRecipe(1),
                    getRecipe(2),
                    getRecipe(3),
                    getRecipe(4),
                ]}
            />
        );

        expect(wrapper).toMatchSnapshot()
    })

    it('Should render empty alert', () => {
        const wrapper = shallow(
            <Recipes
                recipes={[]}
            />
        );

        expect(wrapper).toMatchSnapshot()
    })
})