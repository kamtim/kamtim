import React from 'react';
import {shallow} from 'enzyme';
import {RecipeCard} from "./RecipeCard";

describe('RecipeCard', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <RecipeCard
                title="cool recipe"
                image="https://spoonacular.com/recipeImages/654812-556x370.jpg"
                id={100}
            />
        );

        expect(wrapper).toMatchSnapshot()
    })
})