import React from 'react';
import {cn} from '@bem-react/classname';
import { Card } from 'antd';

import {prefixPath} from '@main/constants/constants';
import './RecipeCard.css';

const cnRecipeCard = cn('RecipeCard');
const { Meta } = Card;

export const RecipeCard = ({title, image, id}) => {
    return (
        <a href={`${prefixPath}/recipe/${id}`}>
            <Card
                className={cnRecipeCard()}
                hoverable
                cover={<img alt="example" src={image} />}
            >
                <Meta title={title} />
            </Card>
        </a>
    )
};