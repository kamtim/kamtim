import React from 'react';
import {shallow} from 'enzyme';
import {Page} from "@main/components/Page/Page";

describe('Page', () => {
    it('Should render component with text children', () => {
        const wrapper = shallow(
            <Page>App</Page>
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('Should render component with component children', () => {
        const wrapper = shallow(
            <Page>
                <div>App</div>
            </Page>
        );

        expect(wrapper).toMatchSnapshot();
    })
});