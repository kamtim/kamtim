import React from 'react';
import {cn} from '@bem-react/classname';

import {Header} from '@main/components/Header/Header';
import './Page.css';

const cnPage = cn('Page');

interface PageProps {
    children: React.ReactChild,
}

export const Page = ({children}: PageProps) => {
    return (
        <div className={cnPage()}>
            <Header />
            <div className={cnPage('Content')}>
                {children}
            </div>
        </div>
    );
};