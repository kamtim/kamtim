import React from 'react';
import { Typography } from 'antd';

import './Header.css';

const { Title } = Typography;

export const Header = () => {
    return (
        <div  className="Header">
            <Title level={4}>kamtim</Title>
            <div className="Example" />
        </div>
    );
};