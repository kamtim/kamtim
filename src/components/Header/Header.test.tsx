import React from "react";
import {Header} from "@main/components/Header/Header";
import {shallow} from 'enzyme';

describe('Header', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Header/>
        );

        expect(wrapper).toMatchSnapshot();
    })
});