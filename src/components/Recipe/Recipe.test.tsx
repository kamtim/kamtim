import React from 'react';
import {shallow} from 'enzyme';
import {Recipe} from "./Recipe";

const recipe: ExtendedRecipe = {
    id: 100,
    image: '',
    imageType: '',
    title: 'Good recipe',

    analyzedInstructions: [],
    summary: '',
    extendedIngredients: [],

    healthScore: 10,
    readyInMinutes: 45,
    servings: 6,

    cheap: false,
    dairyFree: false,
    glutenFree: true,
    vegan: true,
    vegetarian: false,
    veryHealthy: true,
    veryPopular: false,
};

describe('Recipe', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Recipe
                recipe={recipe}
                recipeLoading={false}
            />
        );

        expect(wrapper).toMatchSnapshot()
    });

    it('Should render component with loading', () => {
        const wrapper = shallow(
            <Recipe
                recipe={recipe}
                recipeLoading={true}
            />
        );

        expect(wrapper).toMatchSnapshot()
    });

    it('Should render component with empty recipe', () => {
        const wrapper = shallow(
            <Recipe
                recipe={null}
                recipeLoading={true}
            />
        );

        expect(wrapper).toMatchSnapshot()
    });

    it('Should render component with empty recipe object', () => {
        const wrapper = shallow(
            <Recipe
                recipe={{}}
                recipeLoading={true}
            />
        );

        expect(wrapper).toMatchSnapshot()
    });
});