import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import {Recipe} from "@main/components/Recipe/Recipe";
import {getRecipeRoutine, RECIPE_GET} from "@main/redux/actions";
import {selectIsLoading} from '@main/redux/selectors';

export const RecipeContainer = (props) => {
    const {
        match: {params: {recipeId}},
        recipe,
        recipeLoading,
        getRecipe,
    } = props;

    useEffect(() => {getRecipe(recipeId);}, []);

    return (
        <Recipe recipe={recipe} recipeLoading={recipeLoading} />
    )
};


const mapStateToProps = (state) => {
    const {recipeReducer: {recipe}} = state;

    const recipeLoading = selectIsLoading(state, RECIPE_GET);

    return {
        recipe,
        recipeLoading,
    };
};

const mapDispatchToProps = {
    getRecipe: getRecipeRoutine.trigger,
};

export const RecipeConnected = connect(mapStateToProps, mapDispatchToProps)(RecipeContainer);