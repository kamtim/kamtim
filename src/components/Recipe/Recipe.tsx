import React from 'react';
import {cn} from '@bem-react/classname';
import {Typography, Divider, Spin} from 'antd';

import {Breadcrumb} from "./Breadcrumb/Breadcrumb";
import {IngredientsContainer} from './Ingredients/Ingredients.container';
import {Steps} from './Steps/Steps';
import {Info} from "./Info/Info";
import {Summary} from './Summary/Summary';

import './Recipe.css';

const { Title } = Typography;

export const cnRecipe = cn('Recipe');

export const Recipe = ({recipe, recipeLoading}) => {
    if (!recipe || !recipe.id) {
        return (
            <div className={cnRecipe()}>
                <Title className={cnRecipe('Title')}>
                    Нет рецепта
                </Title>
            </div>
        )
    }

    return (
        <div className={cnRecipe()}>
            {
                recipeLoading ?
                    <Spin className={cnRecipe('Spin')} tip="Loading..." /> :
                    <>
                        <Breadcrumb />
                        <Divider />
                        <Title className={cnRecipe('Title')}>
                            {recipe.title}
                        </Title>
                        <Info recipe={recipe}/>
                        <Divider />
                        <IngredientsContainer
                            ingredients={recipe.extendedIngredients}
                            recipeId={recipe.id}
                        />
                        <Divider />
                        <Steps
                            instructions={recipe.analyzedInstructions}
                            recipeId={recipe.id}
                        />
                        <Divider />
                        <Summary
                            summary={recipe.summary}
                        />
                    </>
            }
        </div>
    )
};