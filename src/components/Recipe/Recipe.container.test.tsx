import React from 'react';
import {mount} from 'enzyme';
import {RecipeContainer} from "./Recipe.container";

const recipe: ExtendedRecipe = {
    id: 100,
    image: '',
    imageType: '',
    title: 'Good recipe',

    analyzedInstructions: [],
    summary: '',
    extendedIngredients: [],

    healthScore: 10,
    readyInMinutes: 45,
    servings: 6,

    cheap: false,
    dairyFree: false,
    glutenFree: true,
    vegan: true,
    vegetarian: false,
    veryHealthy: true,
    veryPopular: false,
};

describe('Recipes', () => {
    const recipeId = 100;
    const getRecipe = jest.fn();

    const wrapper = mount(
        <RecipeContainer
            match={{params: {recipeId}}}
            recipe={recipe}
            recipeLoading={false}
            getRecipe={getRecipe}
        />
    );

    it('Should getRecipe when initialize', () => {
        wrapper.update();
        expect(getRecipe).toHaveBeenCalledWith(recipeId);
    })
})