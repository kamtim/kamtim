import React from 'react';
import {mount} from 'enzyme';
import {IngredientsContainer} from "./Ingredients.container";
import {Ingredients} from "./Ingredients";

import { act } from 'react-dom/test-utils';

const getIngredient: (id: number) => Ingredient = (id) => ({
    id,
    name: `ingredient${id}`,
    original: `1 ingredient${id}`,
    image: `ingredientImage${id}`,
});

describe('Ingredients', () => {
    const wrapper = mount(
        <IngredientsContainer
            recipeId={200}
            ingredients={[
                getIngredient(1),
                getIngredient(2),
                getIngredient(3),
            ]}
        />
    );

    it('call onChange', done => {
        wrapper.find(Ingredients).prop('onChange')(1);

        act(() => {
            wrapper.update();

            setTimeout(() => {
                done();
            }, 2000);
        })
    });

    it('Should check onChange', () => {
        expect(wrapper.find(Ingredients).prop('checked')).toEqual({'1': true});

    });

    it('call onChange with 2', done => {
        wrapper.find(Ingredients).prop('onChange')(2);

        act(() => {
            wrapper.update();

            setTimeout(() => {
                done();
            }, 2000);
        })
    });

    it('Should check onChange with 2', () => {
        expect(wrapper.find(Ingredients).prop('checked')).toEqual({'1': true, '2': true});

    });

    it('call clearSelection', done => {
        wrapper.find(Ingredients).prop('clearSelection')();

        act(() => {
            wrapper.update();

            setTimeout(() => {
                done();
            }, 2000);
        })
    });

    it('Should check clearSelection', () => {
        expect(wrapper.find(Ingredients).prop('checked')).toEqual({});
    });
});