import {Checkbox, Typography, Button} from "antd";
import React from "react";
import {cnRecipe} from "../Recipe";

const {Title} = Typography;

interface IngredientsProps {
    checked: {[key: string]: boolean},
    onChange: (id: number) => void;
    clearSelection: () => void;
    ingredients: Ingredient[],
}

export const Ingredients: React.FC<IngredientsProps> = props => {
    const {checked, onChange, clearSelection, ingredients} = props;

    return (
        <div className={cnRecipe('Ingredients')}>
            <Title className={cnRecipe('StepsTitle')} level={3}>
                Ingredients
                <Button onClick={clearSelection}>Clear all selection</Button>
            </Title>
            {
                ingredients.map(({id, image, original}) => (
                    <div
                        className={cnRecipe('Ingredient', {checked: checked[id] })}
                        key={id}
                    >
                        <Checkbox
                            className={cnRecipe('Checkbox')}
                            checked={checked[id]}
                            onChange={() => onChange(id)}
                        >
                            <img
                                className={cnRecipe('IngredientImage')}
                                src={`https://spoonacular.com/cdn/ingredients_100x100/${image}`}
                                alt="ingredient"
                            />
                            {original}
                        </Checkbox>
                    </div>

                ))
            }
        </div>
    )
};