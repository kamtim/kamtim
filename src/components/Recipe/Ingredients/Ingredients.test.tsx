import React from 'react';
import {shallow, mount} from 'enzyme';
import {Ingredients} from "./Ingredients";

const getIngredient: (id: number) => Ingredient = (id) => ({
    id,
    name: `ingredient${id}`,
    original: `1 ingredient${id}`,
    image: `ingredientImage${id}`,
});

describe('Ingredients', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Ingredients
                onChange={jest.fn()}
                clearSelection={jest.fn()}
                checked={{}}
                ingredients={[
                    getIngredient(1),
                    getIngredient(2),
                    getIngredient(3),
                ]}
            />
        );

        expect(wrapper).toMatchSnapshot()
    });

    it('Should check onChange', () => {
        const onChange = jest.fn();

        const wrapper = mount(
            <Ingredients
                onChange={onChange}
                clearSelection={jest.fn()}
                checked={{}}
                ingredients={[
                    getIngredient(1),
                    getIngredient(2),
                    getIngredient(3),
                ]}
            />
        );

        wrapper.find('.Recipe-Ingredient input').first().simulate('change', {"target": {"checked": true}});

        expect(onChange).toHaveBeenCalledWith(1);
    });

    it('Should check clearSelection', () => {
        const clearSelection = jest.fn();

        const wrapper = mount(
            <Ingredients
                onChange={jest.fn()}
                clearSelection={clearSelection}
                checked={{}}
                ingredients={[
                    getIngredient(1),
                    getIngredient(2),
                    getIngredient(3),
                ]}
            />
        );

        wrapper.find('.Recipe-StepsTitle button').simulate('click');

        expect(clearSelection).toHaveBeenCalled();
    });
});