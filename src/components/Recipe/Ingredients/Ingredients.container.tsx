import React, {useCallback, useState} from "react";
import {Ingredients} from "./Ingredients";

const LOCAL_STORAGE_KEY = 'ingredientsChecked';

const getRecipesChecked = () => JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || {};
const getRecipeChecked = (recipeId) => getRecipesChecked()[recipeId] || {};

export const IngredientsContainer = ({ingredients, recipeId}: {ingredients: Ingredient[], recipeId: number}) => {
    const [checked, setChecked] = useState(getRecipeChecked(recipeId));
    const onChange = useCallback((id) => {
        setChecked(oldChecked => {
            const newRecipeChecked = {...oldChecked, [id]: !oldChecked[id]};

            const {[id]: oldRecipeChecked, ...othersRecipeChecked} = getRecipesChecked();
            localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify({...othersRecipeChecked, [recipeId]: newRecipeChecked}));

            return newRecipeChecked;
        })
    }, []);

    const clearSelection = useCallback(() => {
        setChecked({});

        const {[recipeId]: oldRecipeChecked, ...othersRecipeChecked} = getRecipesChecked();
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(othersRecipeChecked));
    }, []);

    return (
        <Ingredients
            checked={checked}
            onChange={onChange}
            clearSelection={clearSelection}
            ingredients={ingredients}
        />
    )
};