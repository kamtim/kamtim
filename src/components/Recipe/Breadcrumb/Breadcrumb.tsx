import React from "react";
import {Breadcrumb as AntdBreadcrumb} from 'antd';

export const Breadcrumb = () => {
    return (
        <AntdBreadcrumb separator=">">
            <AntdBreadcrumb.Item href="/kamtim">Recipes search</AntdBreadcrumb.Item>
            <AntdBreadcrumb.Item href="">The recipe</AntdBreadcrumb.Item>
        </AntdBreadcrumb>
    )
}