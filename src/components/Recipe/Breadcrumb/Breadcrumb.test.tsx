import React from 'react';
import {shallow} from 'enzyme';
import {Breadcrumb} from "./Breadcrumb";

describe('Breadcrumb', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Breadcrumb />
        );

        expect(wrapper).toMatchSnapshot()
    })
})