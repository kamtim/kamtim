import React from "react";
import {Typography} from 'antd';
import {cnRecipe} from "@main/components/Recipe/Recipe";

const { Title } = Typography;


export const Summary = ({summary}) => {
    return (
        <div className={cnRecipe('Summary')}>
            <Title className={cnRecipe('SummaryTitle')} level={3}>
                Summary
            </Title>
            <div dangerouslySetInnerHTML={{ __html: summary }} />
        </div>
    )
}