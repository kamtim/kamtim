import React from 'react';
import {shallow} from 'enzyme';
import {Summary} from "./Summary";

describe('Summary', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Summary
                summary="<b>Super summary</b>"
            />
        );

        expect(wrapper).toMatchSnapshot()
    })
});