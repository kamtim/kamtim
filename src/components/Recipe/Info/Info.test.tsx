import React from 'react';
import {shallow} from 'enzyme';
import {Info} from "./Info";

const recipe: ExtendedRecipe = {
    id: 11,
    image: '',
    imageType: '',
    title: 'Good recipe',

    analyzedInstructions: [],
    summary: '',
    extendedIngredients: [],

    healthScore: 10,
    readyInMinutes: 45,
    servings: 6,

    cheap: false,
    dairyFree: false,
    glutenFree: true,
    vegan: true,
    vegetarian: false,
    veryHealthy: true,
    veryPopular: false,
};

describe('Info', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Info
                recipe={recipe}
            />
        );

        expect(wrapper).toMatchSnapshot()
    })
});