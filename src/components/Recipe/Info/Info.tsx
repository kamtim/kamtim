import {Alert} from "antd";
import React from "react";
import {cnRecipe} from "@main/components/Recipe/Recipe";

const yesOrNo = (bool) => bool ? 'yes' : 'no';

export const Info = ({recipe}) => {
    return (
        <div className={cnRecipe('Information')}>
            <img
                className={cnRecipe('Image')}
                src={recipe.image}
                alt="recipe"
            />
            <Alert
                className={cnRecipe('InfoBlock')}
                message="Information"
                description={
                    <div>
                        <div>Health score: {recipe.healthScore}</div>
                        <div>Servings: {recipe.servings}</div>
                        <div>Time: {recipe.servings} minutes</div>

                        <div>Cheap: {yesOrNo(recipe.cheap)}</div>
                        <div>Dairy-free: {yesOrNo(recipe.dairyFree)}</div>
                        <div>Gluten-free: {yesOrNo(recipe.glutenFree)}</div>
                        <div>Vegan: {yesOrNo(recipe.vegan)}</div>
                        <div>Vegetarian: {yesOrNo(recipe.vegetarian)}</div>
                        <div>Very healthy: {yesOrNo(recipe.veryHealthy)}</div>
                        <div>Very popular: {yesOrNo(recipe.veryPopular)}</div>
                    </div>
                }
                type="info"
                showIcon
            />
        </div>
    )
}