import React from 'react';
import {shallow} from 'enzyme';
import {Steps} from "./Steps";

const getStep: (number: number) => Step = (number) => ({
    number,
    step: `It is step ${number}`
});

describe('Ingredients', () => {
    it('Should render component', () => {
        const wrapper = shallow(
            <Steps
                instructions={[{
                    steps: [
                        getStep(1),
                        getStep(2),
                        getStep(3),
                        getStep(4),
                    ]
                }]}
                recipeId={100}
            />
        );

        expect(wrapper).toMatchSnapshot()
    });

    it('Should render component with no instructions', () => {
        const wrapper = shallow(
            <Steps
                recipeId={100}
            />
        );

        expect(wrapper).toMatchSnapshot()
    });
});