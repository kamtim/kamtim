import React, {useCallback, useState} from 'react';
import {Typography, Steps as AntdSteps} from 'antd';

import {cnRecipe} from "../Recipe";

const { Step } = AntdSteps;
const { Title } = Typography;

const LOCAL_STORAGE_KEY = 'recipesCurrentStep';

const getRecipesCurrentStep = () => JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || {};
const getRecipeCurrentStep = (recipeId) => getRecipesCurrentStep()[recipeId] || 0;

export const Steps = ({instructions, recipeId}: {instructions?: {steps: Step[]}[], recipeId: number}) => {
    const [currentStep, setCurrentStep] = useState(getRecipeCurrentStep(recipeId));

    const changeHandler = useCallback(current => {
        setCurrentStep(current);

        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify({...getRecipesCurrentStep(), [recipeId]: current}));
    }, []);

    if (!instructions || !instructions.length) {
        return (
            <div className={cnRecipe('Steps')}>
                <Title className={cnRecipe('StepsTitle')} level={3}>
                    No Steps
                </Title>
            </div>
        )
    }

    return (
        <div className={cnRecipe('Steps')}>
            <Title className={cnRecipe('StepsTitle')} level={3}>
                Steps
            </Title>
            <AntdSteps
                size="small"
                current={currentStep}
                onChange={changeHandler}
                direction="vertical"
            >
                {
                    instructions[0].steps.map(step => (
                        <Step
                            className={cnRecipe('Step')}
                            title={`Step ${step.number}`}
                            description={step.step}
                            key={step.number}
                        />
                    ))
                }
            </AntdSteps>
        </div>
    )
}