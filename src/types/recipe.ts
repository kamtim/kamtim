interface Recipe {
    id: number;
    // Image link
    image: string;
    imageType: string;
    title: string;
}

interface Step {
    number: number;
    step: string
}

interface Ingredient {
    id: number;
    name: string;
    original: string;
    image: string;
}

interface ExtendedRecipe extends Recipe {
    analyzedInstructions: {steps: Step[]}[],
    summary: string;
    extendedIngredients: Ingredient[];

    healthScore: number;
    readyInMinutes: number;
    servings: number;

    cheap: boolean;
    dairyFree: boolean;
    glutenFree: boolean;
    vegan: boolean;
    vegetarian: boolean;
    veryHealthy: boolean;
    veryPopular: boolean;
}