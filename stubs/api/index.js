const router = require('express').Router();

router.get('/', (req, res) => {
    res.send('Hi!');
});

router.get('/recipes/complexSearch', (req, res) => {
    let response = require('./recipesComplexSearch');
    res.send(response);
});

router.get('/recipes/autocomplete', (req, res) => {
    let response = require('./recipesAutocomplete');
    res.send(response);
});

router.get('/recipes/:recipeId/information', (req, res) => {
    let response = require('./recipe');
    res.send(response);
});

router.get('/food/ingredients/autocomplete', (req, res) => {
    let response = require('./ingerdientsAutocomplete');
    res.send(response);
});

module.exports = router;
