/**
 * Открыть страницу рецепта
 * И сделать скриншот, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const openRecipePage = ({needCheck, previousSteps} = {}) => {
    cy.visit('http://localhost:8090/kamtim/recipe/749013');

    if (!needCheck) {
        return;
    }

    cy.wait(1000);
    cy.get('.Page').toMatchImageSnapshot();
};

/**
 * Выбирает первый ингредиент
 * И делает скриншот до и после перезагрузки страницы, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const chooseIngredient = ({needCheck, previousSteps} = {}) => {
    if (previousSteps) {
        openRecipePage({previousSteps: true});
    }

    cy.get('.Recipe-Ingredient input[type=checkbox]').first().check();

    if (!needCheck) {
        return;
    }

    cy.wait(1000);
    cy.get('.Recipe-Ingredients').toMatchImageSnapshot();

    cy.reload();
    cy.wait(1000);
    cy.get('.Recipe-Ingredients').toMatchImageSnapshot();
};

/**
 * Выбирает второй шаг
 * И делает скриншот до и после перезагрузки страницы, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const chooseStep = ({needCheck, previousSteps} = {}) => {
    if (previousSteps) {
        openRecipePage({previousSteps: true});
    }

    cy.get('.Recipe-Step:nth-child(2)').click();

    if (!needCheck) {
        return;
    }

    cy.wait(1000);
    cy.get('.Recipe-Steps').toMatchImageSnapshot();

    cy.reload();
    cy.wait(1000);
    cy.get('.Recipe-Steps').toMatchImageSnapshot();
};

describe('Recipe page', () => {
    it('Should render recipe page', () => {
        openRecipePage({needCheck: true});
    });

    it('Should check ingredients', () => {
        chooseIngredient({needCheck: true});
    });

    it('Should choose step', () => {
        chooseStep({needCheck: true});
    });
});