/**
 * Открыть главную страницу
 * И сделать скриншот, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const openMainPage = ({needCheck, previousSteps} = {}) => {
    cy.visit('http://localhost:8090/kamtim');

    if (!needCheck) {
        return;
    }

    cy.get('.Page').toMatchImageSnapshot();
};

/**
 * Ввести текст в инпут поиска
 * И сделать скриншот панели автодополнения, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const typeInInput = ({needCheck, previousSteps} = {}) => {
    if (previousSteps) {
        openMainPage({previousSteps: true});
    }

    cy.get('.RecipesPage-SearchInput input').type('pasta');
    cy.wait(2000);

    if (!needCheck) {
        return;
    }

    cy.get('.Page').toMatchImageSnapshot();
};

/**
 * Переход на страницу через панель автодополнения, клик на первый вариант
 * И сделать скриншот страницы рецепта и проверить url, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const transitionThroughAutocomplete = ({needCheck, previousSteps} = {}) => {
    if (previousSteps) {
        typeInInput({previousSteps: true});
    }

    cy.get('.ant-select-item-option:first-child').click();
    cy.wait(2000);

    if (!needCheck) {
        return;
    }

    cy.url().should('eq', 'http://localhost:8090/kamtim/recipe/749013');
    cy.get('.Page').toMatchImageSnapshot();
};

/**
 * Ввести текст в инпут поиска
 * И сделать скриншот панели автодополнения, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const findRecipes = ({needCheck, previousSteps} = {}) => {
    if (previousSteps) {
        typeInInput({previousSteps: true});
    }

    cy.get('.RecipesPage-SearchInput button').click();
    cy.wait(1000);

    if (!needCheck) {
        return;
    }

    cy.get('.Page').toMatchImageSnapshot({height: 200});
};

/**
 * Перейти на страницу рецепта через первую карту рецепта
 * И сделать скриншот страницы рецепта и проверить url, если нужна проверка
 *
 * @param needCheck - нужно ли проверять этот шаг
 * @param previousSteps - нужно ли выполнятть предыдущие шаги
 */
const transitionThroughCard = ({needCheck, previousSteps} = {}) => {
    if (previousSteps) {
        findRecipes({previousSteps: true});
    }

    cy.get('a[href="/kamtim/recipe/654812"]').click();
    cy.wait(1000);

    if (!needCheck) {
        return;
    }

    cy.url().should('eq', 'http://localhost:8090/kamtim/recipe/654812');
    cy.get('.Page').toMatchImageSnapshot();
};

describe('Main page', () => {
    it('Should render empty page', () => {
        openMainPage({needCheck: true})
    });

    it('Should render autocomplete panel after input typing', () => {
        typeInInput({needCheck: true})
    });

    it('Should go on correct page through autocomplete panel', () => {
        transitionThroughAutocomplete({needCheck: true})
    });

    it('Should render recipe cards after searching', () => {
        findRecipes({needCheck: true, previousSteps: true})
    });

    it('Should go on correct page through recipe card', () => {
        transitionThroughCard({needCheck: true})
    });
});