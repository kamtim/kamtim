module.exports = {
    roots: ['<rootDir>/src'],
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
    moduleNameMapper: {
        "@main/(.*)": "<rootDir>/src/$1",
        '\\.(?:css)$': '<rootDir>/src/test/cssMock.js',
    },
    setupFiles: [
        '<rootDir>/src/test/setup.ts',
    ],
    snapshotSerializers: [
        'enzyme-to-json/serializer',
    ],
    coverageThreshold: {
        global: {
            statements: 80,
            branches: 80,
            functions: 80,
            lines: 80,
        },
    },
    collectCoverageFrom: [
        'src/components/**/*.tsx',
        'src/redux/**/*.ts',
    ]
};